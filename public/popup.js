/*$(document).ready(function() {
      $('.heyyy').addEventListener('click',function(e){
          e.preventDefault();
         window.addEventListener('popstate',function(event){
          if (window.history.state !== {page:1}){
         $('.pop_up').fadeOut();
         $('main').css('filter','none');
         window.history.replaceState({page:0},null,'https://goldenfoxx0810.gitlab.io/5tty/');
        }
        else {
            $('.pop_up').fadeIn();
         $('main').css('filter','blur(5px)');
         window.history.pushState({page:1}, null, '?#form_popup');
        }
         });
      });

      $('.close').click(function(){
         $('.pop_up').fadeOut();
         $('main').css('filter','none');
         window.history.back();
      });
})*/
$('document').ready(function(){
    $('.heyyy').on('click', function(e){      
        // отменяем стандартное действие при клике
        e.preventDefault();
        // Получаем адрес страницы
        let href = $(this).attr('href');
        // Передаем адрес страницы в функцию
        getContent(href, true);
    });
    
    $('.close').click(function(){
         $('.pop_up').fadeOut();
         $('main').css('filter','none');
         window.history.back();
      });
});

// Добавляем обработчик события popstate, 
// происходящего при нажатии на кнопку назад/вперед в браузере  
window.addEventListener("popstate", function(e) {
    // Передаем текущий URL
    getContent(location.pathname, false);
});

// Функция загрузки контента
function getContent(url, addEntry) {
        if(addEntry === true) {
         $('.pop_up').fadeIn();
         $('main').css('filter','blur(5px)');
         window.history.pushState({page:1}, null, '?#form_popup');}
         else {
             $('.pop_up').fadeOut();
             $('main').css('filter','none');
             window.history.replaceState({page:0},null,'https://barneydd.gitlab.io/web10/');
         }
        }
